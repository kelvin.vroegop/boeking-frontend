import {Component, Input, OnInit, Output, EventEmitter} from '@angular/core';
import {Book} from "../../model/Book";
import {BookService} from "../../services/book.service";
import {ActivatedRoute, Router} from "@angular/router";
import {CommonModule} from '@angular/common';
import {BrowserModule} from '@angular/platform-browser';

@Component({
  selector: "app-book-detail",
  templateUrl: 'book-detail.component.html',
  styleUrls: ['./book-detail.component.scss']
})

export class BookDetailComponent implements OnInit {

  @Input() bookId: number;
  @Input() inPopup = false;
  @Output() closePopup = new EventEmitter();
  book: Book = { // borrowed is direct gezet om te zorgen dat devexpress hem altijd als boolean behandelt en niet als string
      borrowed: false,
    } as Book;
  colCountByScreen = {
    xs: 1,
    sm: 1,
    md: 1,
    lg: 1
  };

  constructor(private readonly bookService: BookService,
              private readonly router: Router,
              private readonly route: ActivatedRoute) {
    this.bookId = Number(this.route.snapshot.paramMap.get('bookId'));
  }

  ngOnInit() {
    if (this.bookId) {


      this.bookService.getById(this.bookId).subscribe(book => {
        this.book = book;
      });
    } else {
      this.book = {
        id: 0,
        isbn: "",
        title: "",
        authors: "",
        publishdDate: new Date(),
        borrowed: false,
      } as Book;
    }
  }

  save() {
    if (this.book.id) {
      this.bookService.update(this.book.id, this.book).subscribe(() => this.bookChanged());
    } else {
      this.bookService.add(this.book).subscribe(() => this.bookChanged());
    }
  }

  delete() {
    if (this.bookId != null) {
      this.bookService.delete(this.bookId).subscribe(() => this.bookChanged());
    }
  }

  borrow() {
    if (this.bookId != null) {
      this.bookService.borrow(this.bookId).subscribe(() => this.bookChanged());
    }
  }

  handin() {
    if (this.bookId != null) {
      this.bookService.handin(this.bookId).subscribe(() => this.bookChanged());
    }
  }

  bookChanged() {
    if (this.inPopup) {
      this.closePopup.emit();
    } else {
      this.router.navigate(["books"], {relativeTo: this.route.parent});
    }
  }
}
