import {Component, OnInit} from '@angular/core';
import 'devextreme/data/odata/store';
import {Book} from "../../model/Book";
import {BookService} from "../../services/book.service";
import {ActivatedRoute, Router} from "@angular/router";
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';

@Component({
  templateUrl: 'books-page.component.html'
})

export class BooksPageComponent implements OnInit{
  dataSource: Book[] = [];
  bookId: number = 0;
  showDetail = false;

  constructor(private readonly bookService: BookService,
              private readonly route: ActivatedRoute,
              private readonly router: Router) {
  }
  ngOnInit() {
    this.loadDatasource();
  }

  loadDatasource() {
    this.bookService.getAll().subscribe(books => this.dataSource = books);
  }

  openBook(book: any) {
    this.bookId = book.data.id;
    this.showDetail = true;
  }

  bookChanged() {
    this.showDetail = false;
    this.loadDatasource();
  }
}
