import {HttpClient} from "@angular/common/http";
import {Injectable} from "@angular/core";
import {Book} from "../model/Book";
import {Observable} from "rxjs";
import {environment} from "../../environments/environment";

@Injectable({
  providedIn: 'root',
})
export class BookService {

  private readonly baseUrl = "/api/v1";

  constructor(private readonly httpClient: HttpClient) {
  }

  public getAll(): Observable<Book[]> {
    const url = this.baseUrl + "/books";
    return this.httpClient.get<Book[]>(url);
  }

  public getById(bookId: number): Observable<Book> {
    const url = this.baseUrl + "/books/" + bookId;
    return this.httpClient.get<Book>(url);
  }

  public add(book: Book): Observable<Book> {
    const url = this.baseUrl + "/books";
    return this.httpClient.post<Book>(url, book);
  }

  public update(bookId: number, book: Book): Observable<Book> {
    const url = this.baseUrl + "/books/" + bookId;
    return this.httpClient.put<Book>(url, book);
  }

  public delete(bookId: number): Observable<Map<string, boolean>> {
    const url = this.baseUrl + "/books/" + bookId;
    return this.httpClient.delete<Map<string, boolean>>(url);
  }

  public borrow(bookId: number): Observable<Book> {
    const url = this.baseUrl + "/books/" + bookId + "/borrow";
    return this.httpClient.put<Book>(url, null);
  }

  public handin(bookId: number): Observable<Book> {
    const url = this.baseUrl + "/books/" + bookId + "/handin";
    return this.httpClient.put<Book>(url, null);
  }
}
