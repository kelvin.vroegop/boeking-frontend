export const navigation = [
  {
    text: 'Home',
    path: '/home',
    icon: 'home'
  },
  {
    text: 'Boeken',
    icon: 'folder',
    items: [
      {
        text: 'Nieuw boek',
        path: '/book/0'
      },
      {
        text: 'Boeken',
        path: '/books'
      }
    ]
  }
];
