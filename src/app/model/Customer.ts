export interface Customer {
  id: number;
  name: string;
  localDate: Date;
}
