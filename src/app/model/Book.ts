export interface Book {
  id: number;
  isbn: string;
  title: string;
  authors: string;
  publishdDate: Date;
  borrowed: boolean;
}
